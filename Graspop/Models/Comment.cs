﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class Comment
    {
        [Key]
        public Guid CommentId { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public string Content { get; set; }

        public Guid BookingId { get; set; }
        public Booking Booking { get; set; }
    }
}
