﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class ProductionUnit
    {
        [Key]
        public Guid ProductionUnitId { get; set; }

        [Required]
        public string Name { get; set; }

        public List<BookingProductionUnit> BookingProductionUnits { get; set; }
    }
}
