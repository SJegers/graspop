﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class BookingDressingRoom
    {
        public Guid BookingId { get; set; }
        public Booking Booking { get; set; }

        public Guid DressingRoomId { get; set; }
        public DressingRoom DressingRoom { get; set; }

        public DateTime date { get; set; }
    }
}
