﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class BookingProductionUnit
    {
        public Guid BookingId { get; set; }
        public Booking Booking { get; set; }

        public Guid ProductionUnitId { get; set; }
        public ProductionUnit ProductionUnit { get; set; }

        public DateTime date { get; set; }
    }
}
