﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class Booking
    {
        [Key]
        public Guid BookingId { get; set; }
        
        public Guid BandId { get; set; }
        public Band Band { get; set; }

        public Guid StageId { get; set; }
        public Stage Stage { get; set; }

        public Guid? TentId { get; set; }
        public Tent Tent { get; set; }

        public List<Comment> Comments { get; set; }

        public List<BookingDressingRoom> BookingDressingRooms { get; set; }
        public List<BookingProductionUnit> BookingProductionUnits { get; set; }


        public DateTime Date { get; set; }
        public bool Laundry { get; set; }
        public int BusStock { get; set; }
        public int BandCoolers { get; set; }
        public int GMMCoolers { get; set; }

        public bool AfterShow { get; set; }
        public bool Special { get; set; }
        public bool TakeAwayFood { get; set; }
        public int TruckDrivers { get; set; }

        public bool ExtensionCable { get; set; }
        public int Cable110V { get; set; }


        public int HDWitGr { get; set; }
        public int HDZwartGr { get; set; }
        public int HDZwartKl { get; set; }
        public int Runner { get; set; }
        public int HDWitKl { get; set; }
        public string Doctor { get; set; }
        public int Oxygen { get; set; }
        public bool Kine { get; set; }
        public bool Transport { get; set; }


    }
}
