﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class Band
    {
        [Key]
        public Guid BandId { get; set; }

        [MaxLength(50)]
        [Required]
        [StringLength(50, MinimumLength = 1)]
        [Display(Name = "Band Name")]
        public string Name { get; set; }

        public List<Member> Members { get; set; }

        public List<Booking> Bookings { get; set; }
    }
}
