﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class Member
    {
        [Key]
        public Guid MemberId { get; set; }

        [Required]
        public string Name { get; set; }
        public string email { get; set; }
        public string gsm_nr { get; set; }

        public Guid PositionId { get; set; }
        public Position Position { get; set; }

        public Guid BandId { get; set; }
        public Band Band { get; set; } 
    }
}