﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class DressingRoom
    {
        [Key]
        public Guid DressingRoomId { get; set; }

        [Required]
        public string Name { get; set; }

        public List<BookingDressingRoom> BookingDressingRooms { get; set; }
    }
}
