﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Models
{
    public class Tent
    {
        [Key]
        public Guid TentId { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Booking> Bookings { get; set; }
    }
}
