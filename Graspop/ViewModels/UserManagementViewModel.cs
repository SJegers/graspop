﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.ViewModels
{
    public class UserManagementViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }

        public IList<string> Roles { get; set; }

    }

}
