﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.ViewModels
{
    public class EditUserViewModel
    {
        public string UserId { get; set; }
        public string UserRoleName { get; set; }
        public List<IdentityRole> Roles { get; set; }

        [Required(ErrorMessage = "Please enter the user name")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password   { get; set; }
    }
}
