﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.ViewModels
{
    public class AddUserViewModel
    {
        public AddUserViewModel()
        {
            this.Roles = new List<IdentityRole>();
        }

        [Required]
        public string RoleId { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public List<IdentityRole> Roles { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}
