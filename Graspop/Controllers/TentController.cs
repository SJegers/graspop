﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Graspop.Data;
using Graspop.Models;
using Microsoft.AspNetCore.Authorization;

namespace Graspop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class TentController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TentController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Tent
        public async Task<IActionResult> Index()
        {
            return View(await _context.Tents.ToListAsync());
        }

        // GET: Tent/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tent = await _context.Tents
                .FirstOrDefaultAsync(m => m.TentId == id);
            if (tent == null)
            {
                return NotFound();
            }

            return View(tent);
        }

        // GET: Tent/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tent/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TentId,Name")] Tent tent)
        {
            if (ModelState.IsValid)
            {
                tent.TentId = Guid.NewGuid();
                _context.Add(tent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tent);
        }

        // GET: Tent/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tent = await _context.Tents.FindAsync(id);
            if (tent == null)
            {
                return NotFound();
            }
            return View(tent);
        }

        // POST: Tent/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("TentId,Name")] Tent tent)
        {
            if (id != tent.TentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TentExists(tent.TentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tent);
        }

        // GET: Tent/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tent = await _context.Tents
                .FirstOrDefaultAsync(m => m.TentId == id);
            if (tent == null)
            {
                return NotFound();
            }

            return View(tent);
        }

        // POST: Tent/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var tent = await _context.Tents.FindAsync(id);
            _context.Tents.Remove(tent);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TentExists(Guid id)
        {
            return _context.Tents.Any(e => e.TentId == id);
        }
    }
}
