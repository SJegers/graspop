﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Graspop.ViewModels;

namespace Graspop.Controllers
{
    [Authorize (Roles = "Administrator")]
    public class AdminController : Controller
    {
        private UserManager<IdentityUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;

        public AdminController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> UserManagement()
        {

            var users = this._userManager.Users.ToList();

            List<UserManagementViewModel> AllUsersAndRoles = new List<UserManagementViewModel>();

            foreach (var user in users)
            {
                var model = new UserManagementViewModel
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Roles = await this._userManager.GetRolesAsync(user)
                };

                AllUsersAndRoles.Add(model);
            }


            return View(AllUsersAndRoles);
        }

        public async Task<IActionResult> EditUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if(user == null) {
                return RedirectToAction("UserManagement");
            }
            var roles = _roleManager.Roles;
            var userRole = await this._userManager.GetRolesAsync(user);
            var editUserViewModel = new EditUserViewModel()
            {
                UserId = user.Id,
                UserName = user.UserName,
                UserRoleName = userRole[0],
                Roles = roles.ToList()
            };
            return View(editUserViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel editUserViewModel)
        {
            IdentityUser user = await _userManager.FindByIdAsync(editUserViewModel.UserId);

            if (user != null)
            { 
                if(!string.IsNullOrWhiteSpace(editUserViewModel.Password))
                {
                    await this._userManager.RemovePasswordAsync(user);
                    await this._userManager.AddPasswordAsync(user, editUserViewModel.Password);
                }
                var userRoles = await _userManager.GetRolesAsync(user);
                await this._userManager.RemoveFromRolesAsync(user, userRoles);
                await _userManager.AddToRoleAsync(user, editUserViewModel.UserRoleName);
            }
            else
            {
                ModelState.AddModelError("", "This user can't be found");
            }

            return RedirectToAction("UserManagement");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                    return RedirectToAction("UserManagement");
                else
                    ModelState.AddModelError("", "Something went wrong while deleting this user.");
            }
            else
            {
                ModelState.AddModelError("", "This user can't be found");
            }
            return View("UserManagement");
        }

        public IActionResult RoleManagement()
        {
            var roles = _roleManager.Roles;
            return View(roles);
        }

        public IActionResult AddRole() => View();

        [HttpPost]
        public async Task<IActionResult> AddRole(AddRoleViewModel addRoleViewModel)
        {

            if (!ModelState.IsValid) return View(addRoleViewModel);

            var role = new IdentityRole
            {
                Name = addRoleViewModel.RoleName
            };

            IdentityResult result = await _roleManager.CreateAsync(role);

            if (result.Succeeded)
            {
                return RedirectToAction("RoleManagement");
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(addRoleViewModel);
        }

        public IActionResult AddUser()
        {
            var roles = _roleManager.Roles;
            var addUserViewModel =  new AddUserViewModel()
            {
                Roles = roles.ToList()
            };
            return View(addUserViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(AddUserViewModel addUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("AddUser");
            }

            var user = new IdentityUser()
            {
                UserName = addUserViewModel.UserName
            };

            IdentityResult result = await _userManager.CreateAsync(user, addUserViewModel.Password);

            if (result.Succeeded)
            {
                var newUser = await _userManager.FindByNameAsync(addUserViewModel.UserName);
                var role = await _roleManager.FindByIdAsync(addUserViewModel.RoleId);
                var roleResult = await _userManager.AddToRoleAsync(user, role.Name);

                if (roleResult.Succeeded)
                {
                    return RedirectToAction("UserManagement");
                }

                foreach (IdentityError error in roleResult.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            var roles = _roleManager.Roles;
            addUserViewModel.Roles = roles.ToList();

            return View(addUserViewModel);
        }
    }
}