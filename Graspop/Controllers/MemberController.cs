﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Graspop.Data;
using Graspop.Models;
using Microsoft.AspNetCore.Authorization;

namespace Graspop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class MemberController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MemberController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Member/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var member = await _context.Members
                .Include(m => m.Band)
                .FirstOrDefaultAsync(m => m.MemberId == id);
            if (member == null)
            {
                return NotFound();
            }

            return View(member);
        }

        // GET: Band/1/Member/Create
        public IActionResult Create(Guid? mainId, Guid? subId)
        {
            IEnumerable<Band> bands;

            if (subId == null)
            {
                bands = _context.Bands;
            } else
            {
                bands = _context.Bands.Where(b => b.BandId == subId);
                ViewData["BandId"] = subId;
            }

            ViewData["SelectListBand"] = new SelectList(bands, "BandId", "Name");
            ViewData["PositionId"] = new SelectList(_context.Positions, "PositionId", "Name");

            return View();
        }

        // POST: Member/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MemberId,Name,email,gsm_nr,PositionId,BandId")] Member member)
        {
            if (ModelState.IsValid)
            {
                member.MemberId = Guid.NewGuid();
                _context.Add(member);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Band", new { id = member.BandId });
            }

            var bands = _context.Bands.Where(b => b.BandId == member.BandId);

            ViewData["BandId"] = new SelectList(bands, "BandId", "Name");
            ViewData["PositionId"] = new SelectList(_context.Positions, "PositionId", "Name", member.PositionId);
            return View(member);  
        }

        // GET: Member/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var member = await _context.Members.FindAsync(id);
            if (member == null)
            {
                return NotFound();
            }
            ViewData["BandId"] = new SelectList(_context.Bands.Where(b => b.BandId == member.BandId), "BandId", "Name", member.BandId);
            ViewData["PositionId"] = new SelectList(_context.Positions, "PositionId", "Name", member.PositionId);
            return View(member);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("MemberId,Name,email,gsm_nr,PositionId,BandId")] Member member)
        {
            if (id != member.MemberId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(member);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MemberExists(member.MemberId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Band", new { id = member.BandId });
            }
            ViewData["BandId"] = new SelectList(_context.Bands, "BandId", "Name", member.BandId);
            return View(member);
        }

        // GET: Member/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var member = await _context.Members
                .Include(m => m.Band)
                .FirstOrDefaultAsync(m => m.MemberId == id);
            if (member == null)
            {
                return NotFound();
            }

            return View(member);
        }

        // POST: Member/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var member = await _context.Members.FindAsync(id);
            _context.Members.Remove(member);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Band", member.BandId);
        }

        private bool MemberExists(Guid id)
        {
            return _context.Members.Any(e => e.MemberId == id);
        }
    }
}
