﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Graspop.Data;
using Graspop.Models;
using Microsoft.AspNetCore.Authorization;

namespace Graspop.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class BookingController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BookingController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Booking
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Bookings.Include(b => b.Band).Include(b => b.Stage).Include(b => b.Tent);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Booking/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .Include(b => b.Band)
                .Include(b => b.Stage)
                .Include(b => b.Tent)
                .Include(b => b.Comments)
                .FirstOrDefaultAsync(m => m.BookingId == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // GET: Booking/Create
        public IActionResult Create()
        {
            ViewData["BandId"] = new SelectList(_context.Bands, "BandId", "Name");
            ViewData["StageId"] = new SelectList(_context.Stages, "StageId", "Name");
            ViewData["TentId"] = new SelectList(_context.Tents, "TentId", "Name");
            return View();
        }

        // POST: Booking/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BookingId,BandId,StageId,TentId,Date,Laundry,BusStock,BandCoolers,GMMCoolers,AfterShow,Special,TakeAwayFood,TruckDrivers,ExtensionCable,Cable110V,HDWitGr,HDZwartGr,HDZwartKl,Runner,HDWitKl,Doctor,Oxygen,Kine,Transport")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                booking.BookingId = Guid.NewGuid();
                _context.Add(booking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BandId"] = new SelectList(_context.Bands, "BandId", "Name", booking.BandId);
            ViewData["StageId"] = new SelectList(_context.Stages, "StageId", "Name", booking.StageId);
            ViewData["TentId"] = new SelectList(_context.Tents, "TentId", "Name", booking.TentId);
            return View(booking);
        }

        // GET: Booking/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            ViewData["BandId"] = new SelectList(_context.Bands, "BandId", "Name", booking.BandId);
            ViewData["StageId"] = new SelectList(_context.Stages, "StageId", "Name", booking.StageId);
            ViewData["TentId"] = new SelectList(_context.Tents, "TentId", "Name", booking.TentId);
            return View(booking);
        }

        // POST: Booking/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("BookingId,BandId,StageId,TentId,Date,Laundry,BusStock,BandCoolers,GMMCoolers,AfterShow,Special,TakeAwayFood,TruckDrivers,ExtensionCable,Cable110V,HDWitGr,HDZwartGr,HDZwartKl,Runner,HDWitKl,Doctor,Oxygen,Kine,Transport")] Booking booking)
        {
            if (id != booking.BookingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.BookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BandId"] = new SelectList(_context.Bands, "BandId", "Name", booking.BandId);
            ViewData["StageId"] = new SelectList(_context.Stages, "StageId", "Name", booking.StageId);
            ViewData["TentId"] = new SelectList(_context.Tents, "TentId", "Name", booking.TentId);
            return View(booking);
        }

        // GET: Booking/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .Include(b => b.Band)
                .Include(b => b.Stage)
                .Include(b => b.Tent)
                .FirstOrDefaultAsync(m => m.BookingId == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // POST: Booking/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var booking = await _context.Bookings.FindAsync(id);
            _context.Bookings.Remove(booking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingExists(Guid id)
        {
            return _context.Bookings.Any(e => e.BookingId == id);
        }

        public async Task<IActionResult> EditBookingService(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.Include(b => b.Comments).FirstAsync(b => b.BookingId == id);
            if (booking == null)
            {
                return NotFound();
            }
            List <Comment> comments = new List<Comment>();
            foreach (var comment in booking.Comments)
            {
                if(comment.Type == "SERVICE")
                {
                    comments.Add(comment);
                }
            }
            booking.Comments = comments;
            return View(booking);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBookingService(Guid id, [Bind("BookingId,Laundry,BusStock,BandCoolers,GMMCoolers")] Booking booking)
        {
            if (id != booking.BookingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var b = _context.Bookings.First(a => a.BookingId == booking.BookingId);
                    b.Laundry = booking.Laundry;
                    b.BusStock = booking.BusStock;
                    b.BandCoolers = booking.BandCoolers;
                    b.GMMCoolers = booking.GMMCoolers;
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.BookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Booking", new { id = booking.BookingId });
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> EditBookingCatering(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.Include(b => b.Comments).FirstAsync(b => b.BookingId == id);
            if (booking == null)
            {
                return NotFound();
            }
            List<Comment> comments = new List<Comment>();
            foreach (var comment in booking.Comments)
            {
                if (comment.Type == "CATERING")
                {
                    comments.Add(comment);
                }
            }
            booking.Comments = comments;
            return View(booking);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBookingCatering(Guid id, [Bind("BookingId,AfterShow,Special,TakeAwayFood,TruckDrivers")] Booking booking)
        {
            if (id != booking.BookingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var b = _context.Bookings.First(a => a.BookingId == booking.BookingId);
                    b.AfterShow = booking.AfterShow;
                    b.Special = booking.Special;
                    b.TakeAwayFood = booking.TakeAwayFood;
                    b.TruckDrivers = booking.TruckDrivers;
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.BookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Booking", new { id = booking.BookingId });
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> EditBookingLogistics(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.Include(b => b.Comments).FirstAsync(b => b.BookingId == id);
            if (booking == null)
            {
                return NotFound();
            }
            List<Comment> comments = new List<Comment>();
            foreach (var comment in booking.Comments)
            {
                if (comment.Type == "LOGISTICS")
                {
                    comments.Add(comment);
                }
            }
            booking.Comments = comments;
            return View(booking);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBookingLogistics(Guid id, [Bind("BookingId,ExtensionCable,Cable110V")] Booking booking)
        {
            if (id != booking.BookingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var b = _context.Bookings.First(a => a.BookingId == booking.BookingId);
                    b.ExtensionCable = booking.ExtensionCable;
                    b.Cable110V = booking.Cable110V;
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.BookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Booking", new { id = booking.BookingId });
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> EditBookingSpecials(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.Include(b => b.Comments).FirstAsync(b => b.BookingId == id);
            if (booking == null)
            {
                return NotFound();
            }
            List<Comment> comments = new List<Comment>();
            foreach (var comment in booking.Comments)
            {
                if (comment.Type == "SPECIALS")
                {
                    comments.Add(comment);
                }
            }
            booking.Comments = comments;
            return View(booking);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBookingSpecials(Guid id, [Bind("BookingId,HDWitGr,HDZwartGr,HDZwartKl,Runner,HDWitKl,Doctor,Oxygen,Kine,Transport")] Booking booking)
        {
            if (id != booking.BookingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var b = _context.Bookings.First(a => a.BookingId == booking.BookingId);
                    b.HDWitGr = booking.HDWitGr;
                    b.HDZwartGr = booking.HDZwartGr;
                    b.HDZwartKl = booking.HDZwartKl;
                    b.Runner = booking.Runner;
                    b.HDWitKl = booking.HDWitKl;
                    b.Doctor = booking.Doctor;
                    b.Oxygen = booking.Oxygen;
                    b.Kine = booking.Kine;
                    b.Transport = booking.Transport;
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.BookingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Booking", new { id = booking.BookingId });
            }
            return RedirectToAction("Index", "Home");
        }
    }


}
