﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Graspop.Data;
using Graspop.Models;
using Microsoft.AspNetCore.Authorization;

namespace Graspop.wwwroot
{
    [Authorize(Roles = "Administrator")]
    public class DressingRoomController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DressingRoomController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DressingRoom
        public async Task<IActionResult> Index()
        {
            return View(await _context.DressingRooms.ToListAsync());
        }

        // GET: DressingRoom/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dressingRoom = await _context.DressingRooms
                .FirstOrDefaultAsync(m => m.DressingRoomId == id);
            if (dressingRoom == null)
            {
                return NotFound();
            }

            return View(dressingRoom);
        }

        // GET: DressingRoom/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DressingRoom/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DressingRoomId,Name")] DressingRoom dressingRoom)
        {
            if (ModelState.IsValid)
            {
                dressingRoom.DressingRoomId = Guid.NewGuid();
                _context.Add(dressingRoom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dressingRoom);
        }

        // GET: DressingRoom/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dressingRoom = await _context.DressingRooms.FindAsync(id);
            if (dressingRoom == null)
            {
                return NotFound();
            }
            return View(dressingRoom);
        }

        // POST: DressingRoom/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("DressingRoomId,Name")] DressingRoom dressingRoom)
        {
            if (id != dressingRoom.DressingRoomId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dressingRoom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DressingRoomExists(dressingRoom.DressingRoomId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dressingRoom);
        }

        // GET: DressingRoom/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dressingRoom = await _context.DressingRooms
                .FirstOrDefaultAsync(m => m.DressingRoomId == id);
            if (dressingRoom == null)
            {
                return NotFound();
            }

            return View(dressingRoom);
        }

        // POST: DressingRoom/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var dressingRoom = await _context.DressingRooms.FindAsync(id);
            _context.DressingRooms.Remove(dressingRoom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DressingRoomExists(Guid id)
        {
            return _context.DressingRooms.Any(e => e.DressingRoomId == id);
        }
    }
}
