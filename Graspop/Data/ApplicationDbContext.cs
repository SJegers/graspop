﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Graspop.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Graspop.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public DbSet<Band> Bands { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<DressingRoom> DressingRooms { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<Tent> Tents { get; set; }
        public DbSet<Comment> Comments { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // 1:N Band -> Booking
            modelBuilder.Entity<Band>()
                .HasMany(b => b.Bookings)
                .WithOne();

            modelBuilder.Entity<Booking>()
                .HasOne(p => p.Band)
                .WithMany(b => b.Bookings);

            // 1:N Stage -> Booking
            modelBuilder.Entity<Stage>()
                .HasMany(b => b.Bookings)
                .WithOne();

            modelBuilder.Entity<Booking>()
                .HasOne(p => p.Stage)
                .WithMany(b => b.Bookings);

            // 1:N Tent -> Booking
            modelBuilder.Entity<Tent>()
                .HasMany(b => b.Bookings)
                .WithOne()
                .IsRequired(false);

            modelBuilder.Entity<Booking>()
                .HasOne(p => p.Tent)
                .WithMany(b => b.Bookings)
                .IsRequired(false);

            // 1:N Band -> Member
            modelBuilder.Entity<Band>()
                .HasMany(b => b.Members)
                .WithOne();

            modelBuilder.Entity<Member>()
                .HasOne(p => p.Band)
                .WithMany(b => b.Members);

            // 1:N Member -> Position
            modelBuilder.Entity<Position>()
                .HasMany(b => b.Members)
                .WithOne();

            modelBuilder.Entity<Member>()
                .HasOne(p => p.Position)
                .WithMany(b => b.Members);

            // 1:N Booking -> Comment
            modelBuilder.Entity<Booking>()
                .HasMany(b => b.Comments)
                .WithOne();

            modelBuilder.Entity<Comment>()
                .HasOne(p => p.Booking)
                .WithMany(b => b.Comments);

            // N:N Booking -> DressingRoom
            modelBuilder.Entity<BookingDressingRoom>()
            .HasKey(t => new { t.BookingId, t.DressingRoomId });

            modelBuilder.Entity<BookingDressingRoom>()
                .HasOne(pt => pt.Booking)
                .WithMany(p => p.BookingDressingRooms)
                .HasForeignKey(pt => pt.BookingId);

            modelBuilder.Entity<BookingDressingRoom>()
                .HasOne(pt => pt.DressingRoom)
                .WithMany(t => t.BookingDressingRooms)
                .HasForeignKey(pt => pt.DressingRoomId);

            // N:N Booking -> ProductionUnit
            modelBuilder.Entity<BookingProductionUnit>()
            .HasKey(t => new { t.BookingId, t.ProductionUnitId });

            modelBuilder.Entity<BookingProductionUnit>()
                .HasOne(pt => pt.Booking)
                .WithMany(p => p.BookingProductionUnits)
                .HasForeignKey(pt => pt.BookingId);

            modelBuilder.Entity<BookingProductionUnit>()
                .HasOne(pt => pt.ProductionUnit)
                .WithMany(t => t.BookingProductionUnits)
                .HasForeignKey(pt => pt.ProductionUnitId);

        }
    }
}
