﻿using Graspop.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Graspop.Data
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ApplicationDbContext>>()))
            {
                // Roles
                if (!context.Roles.Any())
                {
                    context.Roles.Add(new IdentityRole() { Name = "Administrator", NormalizedName = "Administrator".ToUpper() });
                    context.Roles.Add(new IdentityRole() { Name = "Reader", NormalizedName = "Reader".ToUpper() });
                    context.SaveChanges();
                }

                // Admin
                if (!context.Users.Any())
                {
                    var newUser = context.Users.Add(new IdentityUser()
                    {
                        UserName = "Admin",
                        NormalizedUserName = "Admin".ToUpper(),
                        PasswordHash = "AQAAAAEAACcQAAAAEK4YPyBaGR1JGOK74DIb7WIw+DkCeDkffPT19vJIOGtWGLrw/IrA3TXeAal69S/8fA==", // = admin
                        SecurityStamp = "VDL6P37W3QGM7E3ON3NWJ4C7BTWSPIBD",
                    });
                    
                    context.UserRoles.Add(new IdentityUserRole<string>()
                    {
                        RoleId = context.Roles.Where(r => r.Name == "Administrator").Select(r => r.Id).FirstOrDefault(),
                        UserId = newUser.Entity.Id
                    });

                    context.SaveChanges();
                }

                // Add bands
                if (!context.Bands.Any())
                {
                    context.Bands.AddRange(
                        new Band
                        {
                            Name = "When Harry Met Sally"
                        },

                        new Band
                        {
                            Name = "Ghostbusters"
                        },

                        new Band
                        {
                            Name = "Ghostbusters 2"
                        },

                        new Band
                        {
                            Name = "Rio Bravo"
                        }
                    );

                    context.SaveChanges();
                }

                // Add stages
                if (!context.Stages.Any())
                {
                    context.Stages.AddRange(
                        new Stage
                        {
                            Name = "Main Stage 01"
                        },

                        new Stage
                        {
                            Name = "Main Stage 02"
                        },

                        new Stage
                        {
                            Name = "Marquee"
                        },

                        new Stage
                        {
                            Name = "Metal Dome"
                        },
                        new Stage
                        {
                            Name = "Jupiler Stage"
                        }
                    );

                    context.SaveChanges();
                }
                // Add Positions
                if (!context.Positions.Any())
                {
                    context.Positions.AddRange(
                        new Position
                        {
                            Name = "Lead Singer"
                        },

                        new Position
                        {
                            Name = "Lead Guitarist"
                        },

                        new Position
                        {
                            Name = "Drummer"
                        },

                        new Position
                        {
                            Name = "Bassist"
                        },
                        new Position
                        {
                            Name = "Rythim Guitarist"
                        },
                        new Position
                        {
                            Name = "Keyboardist / Pianist"
                        },
                        new Position
                        {
                            Name = "Percussionist"
                        },
                        new Position
                        {
                            Name = "Manager"
                        },
                        new Position
                        {
                            Name = "Producer"
                        },
                        new Position
                        {
                            Name = "Groupie"
                        },
                        new Position
                        {
                            Name = "Roadie"
                        },
                        new Position
                        {
                            Name = "Background Vocals"
                        }
                    );

                    context.SaveChanges();
                }
            }
        }
    }
}
